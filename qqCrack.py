#!-*-coding=utf-8-*-
from selenium import webdriver
import glob
import pandas as pd
from scrapy.http import HtmlResponse
from bs4 import BeautifulSoup
import os
import uuid
from detect import test
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
from time import sleep

import logging
logging.basicConfig(
    # filename='log.txt',
    level=logging.INFO,
    format="%(asctime)s %(filename)s [line: %(lineno)d] - %(levelname)s : %(message)s"
)
logger = logging.getLogger("__filename__")


class SliderVerificationCode(object):
    def __init__(self):  # 初始化一些信息
        self.url = 'https://aq.qq.com/cn2/login_limit/login_limit_index?source_id=3215'
        self.chromedriverPath = "drivers/chromedriver.exe"
        self.driver = webdriver.Chrome(executable_path=self.chromedriverPath)
        self.wait = WebDriverWait(self.driver, 20)  # 设置等待时间20秒

        # 存储需要采集的QQ号码集合
        self.usernames = []
        for xls_file in glob.glob(r'data/Excel/orig/*.xls*'):
            self.usernames.extend(list(pd.read_excel(xls_file)['qq'].values))

        # 获取已经处理的qq(如果当前的文件不存在，则新建一个空的文件)
        if not os.path.exists(r'data/Excel/result/qq.csv'):
            qqcsvfile = pd.DataFrame(columns=['qq', '账号信息'])
            qqcsvfile.to_csv(r"data/Excel/result/qq.csv", index=False)

        self.ok_username = list(pd.read_csv(r'data/Excel/result/qq.csv')['qq'].values)
        print("已经采集{}个账号信息".format(len(self.ok_username)))

        # 去除掉已经处理的qq
        self.usernames = list(set(self.usernames).difference(set(self.ok_username)))
        print("需要采集{}个账号信息".format(len(self.usernames)))

        # 存储结果
        self.results = []

    def save_html(self, username):
        with open('htmls/{}.html'.format(username), 'w', encoding='utf-8') as f:
            f.write(self.driver.page_source)

    def parse_result_html(self, username):
        soup = BeautifulSoup(self.driver.page_source, 'html.parser')
        soup.find_all('div', {'class': 'sheet'})
        response = HtmlResponse(url=self.driver.current_url, body=self.driver.page_source, encoding='utf8')

        """
        # 第三种情况
        js = 'return $("#user_account").val()'
        value = self.driver.execute_script(js)
        print(value)
        if value == '您输入的帐号未被冻结':
            print("第二种情况")
        
        """

        """
        # 第二种情况
        js = 'return $("#user_account").val()'
        value = self.driver.execute_script(js)
        print(value)
        if value == '您输入的账号需要登录手机QQ解封':
            print("第二种情况")
        """

        # 第一种情况: 永久冻结
        # 标题

        js = 'return $("#user_account").val()'
        value = self.driver.execute_script(js)
        title = ''
        if not value:
            title = response.css('div.sheet h3::text').extract_first().strip()
            # 表格详细内容
            contents = []
            for tr in response.css('div.sheet tbody>tr'):
                contents.append(tr.css('td::text').extract())

        return value if value else title

    def input_name_password(self, username):  # 输入用户名和密码
        self.driver.get(self.url)
        self.driver.find_element_by_id('user_account').clear()
        self.driver.find_element_by_id('user_account').send_keys(username)
        sleep(0.5)

    def click_login_button(self):  # 点击登录按钮,出现验证码图片  [点击下一步]
        login_button = self.wait.until(EC.element_to_be_clickable((By.ID, 'next_step')))
        login_button.click()
        sleep(2)

    def get_geetest_image(self):  # 获取验证码图片
        self.driver.switch_to.frame(self.driver.find_element_by_id('tcaptcha_iframe'))
        gapimg = self.wait.until(EC.presence_of_element_located((By.CLASS_NAME, 'tc-bg')))
        sleep(0.5)
        gapimg.screenshot(r'./data/captcha/test/captcha.png'.format(uuid.uuid4()))  # 将class为geetest_canvas_bg的区域截屏保存

    def get_move_track(self, gap): # gap = 204
        track = []  # 移动轨迹
        current = 0  # 当前位移
        # 减速阈值
        mid = gap * 4 / 5  # 前4/5段加速 后1/5段减速
        t = 0.2  # 计算间隔
        v = 10  # 初速度
        while current < gap:
            if current < mid:
                a = 3  # 加速度为+3
            else:
                a = -3  # 加速度为-3
            v0 = v  # 初速度v0
            v = v0 + a * t  # 当前速度
            # print("当前速度: ", v)
            move = v0 * t + 1 / 2 * a * t * t  # 移动距离
            current += move  # 当前位移
            track.append(round(move))  # 加入轨迹
        return track

    def move_slider(self, track):
        slider = self.wait.until(EC.presence_of_element_located((By.ID, 'tcaptcha_drag_thumb')))
        ActionChains(self.driver).click_and_hold(slider).perform()
        for x in track:  # 只有水平方向有运动 按轨迹移动
            ActionChains(self.driver).move_by_offset(xoffset=x, yoffset=0).perform()
        sleep(0.5)
        ActionChains(self.driver).release().perform()  # 松开鼠标

    def get_user_info(self, username):
        # 1. 清空验证码图片
        if os.path.exists(r'data\captcha\test\captcha.png'):
            os.remove(r'data\captcha\test\captcha.png')

        # 2. 登录到相关地址，输入账号信息
        self.input_name_password(username)

        # 3. 点击登录
        self.click_login_button()

        # 4. 登录完成之后，判断是否需要弹出滑动验证码
        html = HtmlResponse(url=self.driver.current_url, body=self.driver.page_source, encoding='utf-8')
        iframe = html.css('#tcaptcha_iframe')

        # 5. 分两种情况(页面跳转到详细页面，或者验证码页面)
        # 5.1 如果有滑动验证码，这进行验证码缺口识别的操作
        if iframe:
            logger.info("有验证码")
            self.get_geetest_image()  # 获取验证码图片
            gap = test()
            gap = gap - 60 - 10 + 5 + 1  # 减去滑块左侧距离图片左侧在x方向上的距离 即为滑块实际要移动的距离
            # print(gap)

            track = self.get_move_track(gap)
            # print("移动轨迹", track)
            self.move_slider(track)

        # 5.2 如果没有验证码, 直接解析页面
        else:
            logger.info("没有验证码")
        sleep(2)
        # 6. 解析详细页面
        content = self.parse_result_html(username)
        return username, content

    def main(self):
        for user in self.usernames:
            try:
                print("获取{}的信息..".format(user))
                username, content = self.get_user_info(str(user))
            except Exception as e:
                print(e.args)
                print("请求失败，请添加代理IP后再重试.. 或者20秒钟之后自动再重试")
                sleep(20)
            else:
                print('\t{},{}'.format(username, content))
                # 将采集的数据存储到dataframe中，之后一起存储到文件中
                self.results.append((username, content))

        self.driver.close()

        df = pd.DataFrame(data=self.results, columns=['qq', '账号信息'])
        df.to_csv('data/Excel/result/qq.csv', index=False, mode='a', header=False)


if __name__ == "__main__":
    # torch安装: https://pytorch.org/get-started/locally/
    # pip install torch==1.5.1+cpu torchvision==0.6.1+cpu -f https://download.pytorch.org/whl/torch_stable.html
    QQSlider = SliderVerificationCode()
    QQSlider.main()

