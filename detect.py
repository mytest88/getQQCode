from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *
from PIL import Image
import torch
from torchvision import transforms


def test():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = Darknet("config/yolov3-captcha.cfg", img_size=416).to(device)  # 加载模型
    model.load_state_dict(torch.load("checkpoints/yolov3_ckpt_99.pth", map_location="cuda" if torch.cuda.is_available() else "cpu"))
    model.eval()  # Set in evaluation mode
    classes = load_classes("data/captcha/classes.names")  # Extracts class labels from file

    img_path = r'./data/captcha/test/captcha.png'
    input_img = transforms.ToTensor()(Image.open(img_path).convert('RGB'))

    input_img, _ = pad_to_square(input_img, 0)
    # Resize
    input_img = resize(input_img, 416)
    input_img.unsqueeze_(0)

    with torch.no_grad():

        detections = model(input_img)  # 1,3,416,416
        detections = non_max_suppression(detections, 0.8, 0.4)

    img = np.array(Image.open(img_path))

    if detections is not None:
        detections = rescale_boxes(detections[0], 416, img.shape[:2])
        x_values = []
        for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
            # print("\t+ Label: %s, Conf: %.5f" % (classes[int(cls_pred)], cls_conf.item()))

            box_w = x2 - x1
            box_h = y2 - y1
            # print('bbox', (x1, y1, box_w, box_h))  # (x1, y1) 左上角顶点，box_w 宽，box_h 高
            # print("x轴的中心坐标: ", x1.item() + box_w.item() / 2.0)
            x_values.append(x1.item() + box_w.item()/2.0)
        # print(max(x_values))
        return int(max(x_values))


# test()
